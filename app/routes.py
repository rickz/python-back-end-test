from app import app
from flaskext.mysql import MySQL
from flask import jsonify, request, render_template
import json

mysql = MySQL()
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = ''
app.config['MYSQL_DATABASE_DB'] = 'backendtest'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
mysql.init_app(app)

sql = mysql.connect()
query = sql.cursor()

# CLIENT API / WEBSITE
@app.route('/') 
@app.route('/index')
def index():
    return render_template('index.html')

@app.route('/user')
def user():
    return render_template('user.html')

@app.route('/user_add')
def user_add():
    return render_template('adduser.html')

@app.route('/user_edit')
def user_edit():
    return render_template('edituser.html')

@app.route('/view_login')
def view_login():
    return render_template('login.html')

# REST API
@app.route('/getUser')
def getUser():
    query.execute("SELECT * FROM users")
    data = query.fetchall()
    result = []

    for row in data:
        result.append(
            {
                "id": row[0],
                "nama_lengkap": row[1],
                "nama_panggilan": row[2],
                "alamat": row[3],
                "umur": row[4],
                "pekerjaan": row[5],
                "hobi": row[6],
            }
        )

    return jsonify(result)

@app.route('/getUserById')
def getUserById():
    idUser = request.args.get('id')
    query.execute("SELECT * FROM users WHERE id = %s", (idUser))
    
    data = query.fetchall()
    result = {}

    for row in data:
        result = {
            "id": row[0],
            "nama_lengkap": row[1],
            "nama_panggilan": row[2],
            "alamat": row[3],
            "umur": row[4],
            "pekerjaan": row[5],
            "hobi": row[6],
        }
        
    return jsonify(result)

@app.route('/addUser', methods=['POST'])
def addUser(): 
    paramsJson = request.json
    paramsFormGet = request.form.get

    if paramsJson:
        params = {
            'nama_lengkap': paramsJson['nama_lengkap'],
            'nama_panggilan': paramsJson['nama_panggilan'],
            'alamat': paramsJson['alamat'],
            'umur': paramsJson['umur'],
            'pekerjaan': paramsJson['pekerjaan'],
            'hobi': paramsJson['hobi'],
        }
    else:
        params = {
            'nama_lengkap': paramsFormGet('nama_lengkap'),
            'nama_panggilan': paramsFormGet('nama_panggilan'),
            'alamat': paramsFormGet('alamat'),
            'umur': paramsFormGet('umur'),
            'pekerjaan': paramsFormGet('pekerjaan'),
            'hobi': paramsFormGet('hobi'),
        }

    query.execute(
        "INSERT INTO users (nama_lengkap, nama_panggilan, alamat, umur, pekerjaan, hobi) VALUES(%s, %s, %s, %s, %s, %s)",
        (
            params['nama_lengkap'],
            params['nama_panggilan'],
            params['alamat'],
            params['umur'],
            params['pekerjaan'],
            params['hobi'],
        )
    )
    
    sql.commit()

    query.execute("SELECT * FROM users WHERE nama_lengkap = %s", (params['nama_lengkap']))
    data = query.fetchall()

    if data[0][0] > 0:
        response = {
            "success": "ok",
            "message": "Your data has been successfully submited!"
        }
    else:
        response = {
            "success": "notOk",
            "message": "Your data has not been successfully submited!"
        }

    return jsonify(response)

@app.route('/editUser', methods=['POST'])
def editUser(): 
    paramsJson = request.json
    paramsFormGet = request.form.get

    if paramsJson:
        params = {
            'id': paramsJson['id'],
            'nama_lengkap': paramsJson['nama_lengkap'],
            'nama_panggilan': paramsJson['nama_panggilan'],
            'alamat': paramsJson['alamat'],
            'umur': paramsJson['umur'],
            'pekerjaan': paramsJson['pekerjaan'],
            'hobi': paramsJson['hobi'],
        }
    else:
        params = {
            'id': paramsFormGet('id'),
            'nama_lengkap': paramsFormGet('nama_lengkap'),
            'nama_panggilan': paramsFormGet('nama_panggilan'),
            'alamat': paramsFormGet('alamat'),
            'umur': paramsFormGet('umur'),
            'pekerjaan': paramsFormGet('pekerjaan'),
            'hobi': paramsFormGet('hobi'),
        }
    query.execute(
        "UPDATE users SET nama_lengkap = %s, nama_panggilan = %s, alamat = %s, umur = %s, pekerjaan = %s, hobi = %s WHERE id = %s",
        (
            params['nama_lengkap'],
            params['nama_panggilan'],
            params['alamat'],
            params['umur'],
            params['pekerjaan'],
            params['hobi'],
            params['id'],
        )
    )
    
    sql.commit()
    response = {
        "success": "ok",
        "message": "Your data has been successfully updated!"
    }

    return jsonify(response)

@app.route('/deleteUser')
def deleteUser():
    idUser = request.args.get('id')
    query.execute("DELETE FROM users WHERE id = %s", (idUser))
    
    sql.commit()

    query.execute("SELECT * FROM users WHERE id = %s", (idUser))
    data = query.fetchall()
    
    if data:
        response = {
            "success": "notOk",
            "message": "Your data has not been successfully deleted!"
        }
    else:
        response = {
            "success": "ok",
            "message": "Your data has been successfully deleted!"
        }
        
    return jsonify(response)

@app.route('/login', methods=['POST'])
def login(): 
    paramsJson = request.json
    paramsFormGet = request.form.get

    if paramsJson:
        params = {
            'email': paramsJson['email'],
            'password': paramsJson['password'],
        }
    else:
        params = {
            'email': paramsFormGet('email'),
            'password': paramsFormGet('password'),
        }

    query.execute("SELECT * FROM login WHERE email = %s AND password = %s", (params['email'], params['password']))
    
    data = query.fetchall()
    if data:
        response = {
            "success": "ok",
            "message": "Success Login!"
        }
    else:
        response = {
            "success": "notOk",
            "message": "Not success login!"
        }

    return jsonify(response)